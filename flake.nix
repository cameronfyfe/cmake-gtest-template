{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    gtest = {
      url = "github:google/googletest?ref=release-1.11.0";
      flake = false;
    };
  };

  outputs = inputs @ { self, ... }:
    (inputs.flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let

        pkgs = import inputs.nixpkgs {
          inherit system;
        };

        nativeBuildInputs = with pkgs; [
          cmake
          git
          gcc
          gnumake
          util-linux
        ];

        src = ./.;

        preBuild = ''
          sudo rm -rf extern/gtest || true
          cp -ar ${inputs.gtest} extern/gtest
        '';

      in
      rec {

        devShell = pkgs.mkShell {
          inherit nativeBuildInputs;
          shellHook = ''
            ${preBuild}
            export PATH="$(pwd)/build/release:$PATH"
          '';
        };

        packages = {

          dev = devShell;

          default = pkgs.stdenv.mkDerivation rec {
            name = "cmake-gtest-template";
            inherit src nativeBuildInputs preBuild;
            phases = [ "unpackPhase" "buildPhase" "installPhase" ];
            buildPhase = ''
              runHook preBuild
              make release
            '';
            installPhase = ''
              mkdir -p $out/bin
              cp build/release/${name} $out/bin/
            '';
          };

          tests = pkgs.stdenv.mkDerivation rec {
            name = "cmake-gtest-template-tests";
            inherit src nativeBuildInputs preBuild;
            phases = [ "unpackPhase" "buildPhase" "installPhase" ];
            buildPhase = ''
              runHook preBuild
              make test
            '';
            installPhase = ''
              cp TestReport.txt $out
            '';
          };

        };

      }));
}
