.PHONY: default
default: release

N ?= 100

# Build directory
BUILD_DIR = build
BUILD_DEBUG = $(BUILD_DIR)/debug
BUILD_RELEASE = $(BUILD_DIR)/release

TEST_REPORT = TestReport.txt

.PHONY: debug
debug: BUILD = $(BUILD_DEBUG)
debug: BUILD_TYPE = Debug
debug: _build

.PHONY: release
release: BUILD = $(BUILD_RELEASE)
release: BUILD_TYPE = Release
release: _build

CMAKE_FLAGS = -D CMAKE_BUILD_TYPE=$(BUILD_TYPE)

.PHONY: _build
_build:
	cmake -E make_directory $(BUILD)
	cmake -E chdir $(BUILD) cmake $(CMAKE_FLAGS) ../..
	cmake --build $(BUILD)

.PHONY: run
run: release
	$(BUILD_RELEASE)/cmake-gtest-template $(N)

.PHONY: test
test: release
	script -efq $(TEST_REPORT) -c "$(BUILD_RELEASE)/unit-tests"

# Create gcroot file that preserves dev environment
.PHONY: dev
dev:
	nix build .#$@ -o .$@

.PHONY: clean
clean:
	rm -rf .dev $(BUILD_DIR) $(TEST_REPORT)