#include <iostream>
#include "gtest/gtest.h"

#include "primes.h"

TEST(PrimesTesting, FirstPrimeIs_2)
{
  Primes p;

	EXPECT_EQ(p.NextNumber(), 2);
}

TEST(PrimesTesting, FirstPrimeIs_2_WithReset)
{
  Primes p;
  p.Reset();

	EXPECT_EQ(p.NextNumber(), 2);
}

TEST(PrimesTesting, ThousandthPrimeIs_7919)
{
  Primes p;

  for (int i=1; i<1000; i++) p.NextNumber();

	EXPECT_EQ(p.NextNumber(), 7919);
}

TEST(PrimesTesting, ThousandthPrimeIs_7919_WithReset)
{
  Primes p;
  p.Reset();

  for (int i=1; i<1000; i++) p.NextNumber();

	EXPECT_EQ(p.NextNumber(), 7919);
}

TEST(PrimesTesting, TenThousandthPrimeIs_104729)
{
  Primes p;

  for (int i=1; i<10000; i++) p.NextNumber();

	EXPECT_EQ(p.NextNumber(), 104729);
}
