#include <iostream>
#include "gtest/gtest.h"

#include "fibonacci.h"

TEST(FibonacciTesting, FirstFibonacciIs_1)
{
  Fibonacci f;

	EXPECT_EQ(f.NextNumber(), 1);
}

TEST(FibonacciTesting, FirstFibonacciIs_1_WithReset)
{
  Fibonacci f;
  f.Reset();

	EXPECT_EQ(f.NextNumber(), 1);
}

TEST(FibonacciTesting, SecondFibonacciIs_1)
{
  Fibonacci f;

  for (int i=1; i<2; i++) f.NextNumber();

	EXPECT_EQ(f.NextNumber(), 1);
}

TEST(FibonacciTesting, ThirdFibonacciIs_2)
{
  Fibonacci f;

  for (int i=1; i<3; i++) f.NextNumber();

	EXPECT_EQ(f.NextNumber(), 2);
}

TEST(FibonacciTesting, FourthFibonacciIs_3)
{
  Fibonacci f;

  for (int i=1; i<4; i++) f.NextNumber();

	EXPECT_EQ(f.NextNumber(), 3);
}

TEST(FibonacciTesting, TenthFibonacciIs_55)
{
  Fibonacci f;

  for (int i=1; i<10; i++) f.NextNumber();

	EXPECT_EQ(f.NextNumber(), 55);
}

TEST(FibonacciTesting, TenthFibonacciIs_55_WithReset)
{
  Fibonacci f;
  f.Reset();

  for (int i=1; i<10; i++) f.NextNumber();

	EXPECT_EQ(f.NextNumber(), 55);
}