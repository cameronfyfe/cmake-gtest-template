# Use Ubuntu 20.04 image
FROM ubuntu:20.04

# Install gcc, make, git, wget
RUN apt update && apt -y install build-essential git wget

# Install cmake 3.19
RUN wget https://github.com/Kitware/CMake/releases/download/v3.19.0/cmake-3.19.0-Linux-x86_64.sh -q -O /tmp/install.sh \
    && chmod u+x /tmp/install.sh \
    && mkdir /usr/bin/cmake \
    && /tmp/install.sh --skip-license --prefix=/usr/bin/cmake \
    && rm /tmp/install.sh
ENV PATH="/usr/bin/cmake/bin:${PATH}"
