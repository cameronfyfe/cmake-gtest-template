#include "primes.h"

Primes::Primes()
{
  Reset();
}

unsigned long Primes::NextNumber()
{
  while (IsPrime(++_last_prime) == false);
  
  return _last_prime;
}

void Primes::Reset()
{
  _name_of_sequence = "Prime Numbers";
  _last_prime = 0;
}

bool Primes::IsPrime(unsigned long n)
{
  if (n == 0 || n == 1)
  {
    return false;
  }

  for (unsigned long i=2; i <= n/2; i++)
  {
    if (n % i == 0)
    {
      return false;
    }
  }
  
  return true;
}