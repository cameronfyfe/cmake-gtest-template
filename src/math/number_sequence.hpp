#ifndef NUMBER_SEQUENCE_H_
#define NUMBER_SEQUENCE_H_

#include <iostream>

template <typename T>
class NumberSequence
{
  public:
    virtual T NextNumber();
    virtual void Reset();

    const char *NameOfSequence() const
    {
      return _name_of_sequence;
    }

    void Print(unsigned n)
    {
      while (n--)
      {
        std::cout << NextNumber() << " ";
      }
      std::cout << std::endl;
    }
  
  protected:
    const char *_name_of_sequence;
};

#endif // NUMBER_SEQUENCE_H_