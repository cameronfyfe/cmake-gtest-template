#ifndef PRIMES_H_
#define PRIMES_H_

#include "number_sequence.hpp"

class Primes: public NumberSequence<unsigned long>
{
  public:
    Primes();
    unsigned long NextNumber();
    void Reset();
    
  private:
    unsigned _last_prime;

    bool IsPrime(unsigned long n);
};

#endif // PRIMES_H_