#ifndef FIBONACCI_H_
#define FIBONACCI_H_

#include "number_sequence.hpp"

class Fibonacci: public NumberSequence<unsigned long>
{
  public:
    Fibonacci();
    unsigned long NextNumber();
    void Reset();

  private:
    unsigned long _fib_n_minus1;
    unsigned long _fib_n_minus2;
    unsigned _n;
};

#endif // FIBONACCI_H_