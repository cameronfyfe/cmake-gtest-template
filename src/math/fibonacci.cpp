#include "fibonacci.h"

Fibonacci::Fibonacci()
{
  _name_of_sequence = "Fibonacci Numbers";
  Reset();
}

unsigned long Fibonacci::NextNumber()
{
  const unsigned long fib =
    (_n == 0) ? 1 :
    (_n == 1) ? 1 :
    _fib_n_minus1 + _fib_n_minus2
  ;

  _n++;
  _fib_n_minus2 = _fib_n_minus1;
  _fib_n_minus1 = fib;

  return fib;
}

void Fibonacci::Reset()
{
  _n = 0;
}