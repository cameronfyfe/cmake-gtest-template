#include <iostream>
#include "fibonacci.h"
#include "primes.h"
#include "number_sequence.hpp"

int main(int argc, char *argv[])
{
  unsigned n = (argc >= 2) ? std::stoul(argv[1], nullptr, 0) : 100;

  NumberSequence<unsigned long> *sequences[] = { new Primes(), new Fibonacci() };

  for (auto sequence : sequences)
  {
    std::cout << std::endl << "First " << n << " " << sequence->NameOfSequence() << ":" << std::endl;
    sequence->Print(n);
  }

  return 0;
}